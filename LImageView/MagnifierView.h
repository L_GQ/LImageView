//
//  MagnifierView.h
//  文字识别扫描
//
//  Created by LGQ on 2020/8/27.
//  Copyright © 2020 上海创酷信息科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MagnifierView : UIWindow

/// 触摸点
@property (nonatomic, assign) CGPoint touchPoint;
/// <#Description#>
@property (nonatomic, strong) UIView *renderView;

- (void)showMagnifierView;

@end

NS_ASSUME_NONNULL_END
