//
//  MagnifierView.m
//  文字识别扫描
//
//  Created by LGQ on 2020/8/27.
//  Copyright © 2020 上海创酷信息科技有限公司. All rights reserved.
//

#import "MagnifierView.h"

static CGFloat const magnifierWH = 130;

@interface MagnifierView ()

@end

@implementation MagnifierView

- (instancetype)init {
    
    if (self = [super init]) {
        
        self.windowLevel = UIWindowLevelAlert + 1;
        
        self.layer.contentsScale = [[UIScreen mainScreen] scale];
        self.layer.delegate = self;
        [self layoutUI];
    }
    
    return self;
}

- (void)layoutUI {
    
    self.backgroundColor = [UIColor whiteColor];
    
    CGFloat height = [[UIApplication sharedApplication].keyWindow safeAreaInsets].top;
    
    self.frame = CGRectMake(5, 30 + height, magnifierWH, magnifierWH);
    
    self.layer.cornerRadius = magnifierWH/2;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 1;
    self.clipsToBounds = YES;
    
//    // 中心十字
//    CAShapeLayer *layer = [CAShapeLayer layer];
//    layer.strokeColor = HEXColor(COLOR_MainColor).CGColor;
//    layer.lineWidth = 2;
//    layer.frame = self.bounds;
//
//    //虚线路径
//    CGMutablePathRef path = CGPathCreateMutable();
//    CGPathMoveToPoint(path, nil, magnifierWH/2 - 9, magnifierWH/2);
//    CGPathAddLineToPoint(path, nil, magnifierWH/2 + 9, magnifierWH/2);
//    CGPathAddLineToPoint(path, nil, magnifierWH/2, magnifierWH/2);
//    CGPathAddLineToPoint(path, nil, magnifierWH/2, magnifierWH/2 - 9);
//    CGPathAddLineToPoint(path, nil, magnifierWH/2, magnifierWH/2 + 9);
//    CGPathAddLineToPoint(path, nil, magnifierWH/2, magnifierWH/2);
//
//    layer.path = path;
//    CGPathRelease(path);
//
//    [self.layer addSublayer:layer];
}

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
    
    //提前位移半个长宽的坑
    CGContextTranslateCTM(ctx, self.frame.size.width * 0.5, self.frame.size.height * 0.5);
    CGContextScaleCTM(ctx, 2, 2);
    //再次位移后就可以把触摸点移至self.center的位置
    CGContextTranslateCTM(ctx, -1 * (self.touchPoint.x), -1 * (self.touchPoint.y));
    [self.renderView.layer renderInContext:ctx];
}

#pragma mark - 事件、方法
- (void)setTouchPoint:(CGPoint)touchPoint {
    
    _touchPoint = touchPoint;
    
    [self.layer setNeedsDisplay];
    
//    if (CGRectContainsPoint(self.frame, touchPoint)) {
//
//        CGRect rect = self.frame;
//
//        if (rect.origin.x < [UIScreen mainScreen].bounds.size.width/2) {
//
//            rect.origin.x = [UIScreen mainScreen].bounds.size.width - 5 - CGRectGetWidth(rect);
//        } else {
//
//            rect.origin.x = 5;
//        }
//
//        self.frame = rect;
//    }
}

- (void)showMagnifierView {
    
//    [self makeKeyAndVisible];
    self.hidden = NO;
}


#pragma mark - init UI

@end
