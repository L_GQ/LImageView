//
//  ViewController.m
//  LGQImageView
//
//  Created by LGQ on 2020/7/28.
//  Copyright © 2020 上海创酷信息科技有限公司. All rights reserved.
//

#import "ViewController.h"

#import "LImageView.h"

@interface ViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet LImageView *imageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.imageView.pointColor = [UIColor orangeColor];
    self.imageView.lineColor = [UIColor orangeColor];
//    self.imageView.minClipWidthAndHeight = 30;
}

- (IBAction)selectImage:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

- (IBAction)confirm:(id)sender {
    
    self.imageView.originImage = [self.imageView currentImage];
    
    [self.imageView resetAreaViewRect];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        self.imageView.originImage = [image copy];
        
        [self.imageView resetAreaViewRect];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
