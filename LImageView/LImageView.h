//
//  LImageView.h
//  LGQImageView
//
//  Created by LGQ on 2020/7/28.
//  Copyright © 2020 上海创酷信息科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LImageView : UIView

/// 原图
@property (nonatomic, copy) UIImage *originImage;
/// 拖动点颜色
@property (nonatomic, copy) UIColor *pointColor;
/// 裁剪边界线颜色
@property (nonatomic, copy) UIColor *lineColor;
/**
 裁剪区域最小宽高
 默认100，最大值图片宽度(防止截取到图片之外的部分)，最小值100(防止拖动点重合导致拖动错误的点)
 */
@property (nonatomic, assign) CGFloat minClipWidthAndHeight;
/// 图片视图
@property (nonatomic, strong) UIImageView *imageView;

/// <#Description#>
@property (nonatomic, assign, readonly) CGFloat currentScale;

/// 获取裁剪后的图片
- (UIImage *)currentImage;

/// 设置裁剪区域
/// @param points 8个顶点的坐标
- (void)setAreaPathWithPoints:(NSArray *)points;

- (void)setAnonateAreaRect:(CGRect)rect;

- (void)setScaleForScrollView:(CGFloat)scale;

/// 获取当前裁剪的坐标点
- (NSArray *)getCurrentPathPoints;

/// 重置裁剪区域
- (void)resetAreaViewRect;

@end


@interface AreaView : UIView

/// 可拖动区域中心点
@property (nonatomic, strong) CAShapeLayer *centerLayer;

/// 中心点连接的点1
@property (nonatomic, weak) AreaView *relationView1;
/// 中心点连接的点2
@property (nonatomic, weak) AreaView *relationView2;

@end



@interface UIImage (ClipImageHandler)

// 根据path在rect范围内切割图片
- (UIImage*)clipWithPath:(UIBezierPath*)path rect:(CGRect)clipRect;

@end

NS_ASSUME_NONNULL_END
