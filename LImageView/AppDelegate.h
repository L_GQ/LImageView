//
//  AppDelegate.h
//  LImageView
//
//  Created by LGQ on 2020/7/30.
//  Copyright © 2020 上海创酷信息科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

